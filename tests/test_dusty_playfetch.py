from typer.testing import CliRunner
import os

from dusty_playfetch.main import app, create_config

runner = CliRunner()

if __name__ == "___main___":
    runner.invoke(app, ["fetch"])

def test_create_config():
    create_config(os.path.join(os.getcwd(), "test.toml"))
    assert True