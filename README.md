# Playlist Fetcher

Fetches playlists (either one-off or indexes them for later updates) using youtube-dl.

## Sources
- https://github.com/yt-dlp/yt-dlp
- https://github.com/coletdjnz/yt-dlp-get-pot
- https://github.com/Brainicism/bgutil-ytdlp-pot-provider

## Updating

```bash
# https://github.com/yt-dlp/yt-dlp.git
uv lock --upgrade
uv sync --upgrade
uv pip install . --system
```

## Settings

```bash
uv run ".\dusty_playfetch\cli_to_api.py" -S 'res:1080' --extractor-args "youtubetab:approximate_date" -o "%(playlist)s/%(playlist_index)03d - %(title)s [%(id)s].%(ext)s" -P "temp:.playfetch/temp" --embed-metadata --embed-subs --sponsorblock-mark all --sleep-requests 1.25 --sleep-interval 60 --max-sleep-interval 90 --mtime --break-per-input --no-progress --no-color --no-abort-on-error
```

For the playlist. For original source, go to [cli_to_api.py](https://github.com/yt-dlp/yt-dlp/blob/master/devscripts/cli_to_api.py)

## TODO

- [ ] https://github.com/yt-dlp/yt-dlp/issues/1918#issuecomment-988924657
