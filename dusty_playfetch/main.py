import logging
import os
import re
import shlex
import sqlite3
from datetime import datetime
from pathlib import Path
from typing import List, Mapping, Optional

import tomlkit
import typer
import yt_dlp
from rich.console import Console, Group
from rich.filesize import decimal
from rich.live import Live
from rich.logging import RichHandler
from rich.markup import escape
from rich.panel import Panel
from rich.progress import (
    BarColumn,
    DownloadColumn,
    MofNCompleteColumn,
    Progress,
    TaskID,
    TaskProgressColumn,
    TextColumn,
    TimeElapsedColumn,
    TimeRemainingColumn,
    TransferSpeedColumn,
)
from rich.theme import Theme
from typing_extensions import Annotated
from yt_dlp.postprocessor import PostProcessor
from yt_dlp.utils import (
    DownloadCancelled,
    DownloadError,
    YoutubeDLError,
    parse_filesize,
)

from dusty_playfetch.cli_to_api import cli_to_api


class SchemaError(Exception):
    pass


class NotAPlaylist(YoutubeDLError):
    msg: str

    def __init__(self, url: str):
        super().__init__(f"Url {url} not a playlist")


class DownloadLogger(logging.LoggerAdapter):
    def process(self, msg: str, kwargs):
        msg, kwargs = super().process(msg, kwargs)
        # Removes unnecessary dangles on message
        return msg.removeprefix("[debug] ").removeprefix("ERROR: "), kwargs

    def debug(self, msg: str, *args, **kwargs):
        # Properly delegates debug/info calls to the underlying logger.
        if msg.startswith("[debug] "):
            super().debug(msg, *args, **kwargs)
        else:
            super().info(msg, *args, **kwargs)


class CheckSizeLimitPP(PostProcessor):
    print_limit: str | None
    limit: int | None

    def __init__(self, downloader=None):
        super().__init__(downloader)

    def run(self, information):
        if self.limit is not None and self.limit > 0:
            filename = information["filename"]
            parent_folder = Path(os.getcwd(), filename).parent
            size = (
                sum(
                    os.path.getsize(f)
                    for f in map(
                        lambda x: Path(parent_folder, x),
                        os.listdir(parent_folder),
                    )
                    if os.path.isfile(f)
                )
                if os.path.exists(parent_folder)
                else 0
            )
            self.write_debug(
                f"Current playlist folder is {decimal(size)}",
            )
            if size > self.limit:
                raise DownloadCancelled(
                    f"Reached {self.print_limit} limit for {information['playlist']}"
                )
        else:
            self.write_debug("No limit set, skipping playlist folder size check")
        return [], information

    def set_limit(self, limit: str | None):
        self.print_limit = limit
        self.limit = parse_bytes(limit)
        self.to_screen(f"Playlist folder limit set: {self.print_limit}")


class UpdateUploadDate(PostProcessor):
    key: str | None
    con: sqlite3.Connection

    def __init__(self, con: sqlite3.Connection, downloader=None):
        super().__init__(downloader)
        self.con = con

    def run(self, information):
        date = get_max_upload_date([information])
        if self.key is not None and date is not None:
            self.write_debug(f"Current playlist last upload date: {date}")
            # update db's last modified upload date
            with self.con:
                query = """SELECT date FROM playlists
                            WHERE key = ?"""
                (prev_date,) = self.con.execute(query, (self.key,)).fetchone()
                self.write_debug(f"Previous playlist last upload date: {prev_date}")
                (new_date,) = (
                    self.con.execute(
                        """UPDATE playlists
                            SET date = COALESCE(MAX(:date, :prev_date), :date)
                            WHERE key = :key""",
                        {"date": date, "prev_date": prev_date, "key": self.key},
                    )
                    .execute(query, (self.key,))
                    .fetchone()
                )
                if prev_date != new_date:
                    self.to_screen(f"Updating playlist last upload date to {date}")
                else:
                    self.to_screen("Playlist last upload date not modified")
        return [], information

    def set_key(self, key: str | None):
        self.key = key


def get_max_upload_date(entries):
    """return max upload date from entry set"""

    def get_upload_date(entry):
        """extracts date from entry info_dict"""
        if entry is None:
            return None
        if "upload_date" in entry:
            ud = entry["upload_date"]
            # YYYYMMDD, negative quotients because YYYY can be bigger than 4 digits, eventually
            return datetime(int(ud[:-4]), int(ud[-4:-2]), int(ud[-2:])).date()
        elif "timestamp" in entry:
            # fallback unix timestamp
            timestamp = entry["timestamp"]
            return (
                datetime.utcfromtimestamp(timestamp).date()
                if timestamp is not None
                else None
            )
        else:
            return None

    dates = filter(lambda x: x is not None, map(get_upload_date, entries))
    return max(dates, default=None)


class RunCallbackPP(PostProcessor):
    cb: callable

    def __init__(self, cb: callable, name: str | None = None):
        PostProcessor.__init__(self)
        self.cb = cb
        self.PP_NAME = f"{self.PP_NAME}+{name if name is not None else cb.__name__}"

    def run(self, information):
        new_information = self.cb(information)
        return [], new_information if new_information is not None else information


MIGRATION = 1
FOLDER = ".playfetch"
ARCHIVE = "archive.txt"
DATABASE = "playlists.sqlite"
CONFIG = "config.toml"

console = Console(theme=Theme({"logging.level.warning": "bright_yellow"}))
print = console.print  # noqa: F811

logging.basicConfig(
    level=logging.NOTSET,
    format="%(message)s",
    datefmt="[%X]",
    handlers=[
        # logging.FileHandler(filename="log.txt", encoding="utf8"),
        RichHandler(rich_tracebacks=True, tracebacks_suppress=[typer], console=console),
    ],
)

log = logging.getLogger()
app = typer.Typer()
# disable rich's highlighting by default
download_logger = DownloadLogger(log, {"highlighter": None})


BYTES_VALIDATE = r"(?i)^(-?\d+(?:\.\d+)?)([kMGTPEZY](?:iB|B)?)?$"


def bytes_validate(value: str | None):
    if value is None:
        return value
    if re.match(BYTES_VALIDATE, value) is None:
        raise typer.BadParameter("Must be number followed by 'kMGTPEZY'")
    return value


def get_config_path(path: str, item: str | None = None):
    config_path = Path(path, FOLDER)
    return Path(config_path, item) if item is not None else config_path


state = {
    "verbose": 0,
}


@app.callback(no_args_is_help=True)
def main(
    verbose: Annotated[
        int, typer.Option("--verbose", "-v", count=True, show_default=False)
    ] = 0,
):
    """Uses yt-dlp to fetch playlists, by last-updated order."""
    match verbose:
        case 0:
            log.setLevel(logging.CRITICAL)
        case 1:
            log.setLevel(logging.ERROR)
        case 2:
            log.setLevel(logging.INFO)
        case _:
            log.setLevel(logging.NOTSET)

    state["verbose"] = verbose


@app.command()
def init(
    force: Annotated[bool, typer.Option()] = False,
):
    """Initiates playfetch onto the current directory."""
    root = os.getcwd()
    os.makedirs(get_config_path(root), exist_ok=True)

    if not init_database(get_config_path(root, DATABASE), force):
        return
    if not init_config(get_config_path(root, CONFIG), force):
        return
    print("[green]Fetcher configuration initialized.")


def init_database(path: str, force: bool = False):
    """Creates the config database."""
    con = sqlite3.connect(path)
    try:
        tables = bool(
            con.execute(
                "SELECT name FROM sqlite_master WHERE type='table' AND name=?",
                ("playlists",),
            ).fetchone()
        )
        (version,) = con.execute("PRAGMA user_version").fetchone()
        if force or not tables:
            create_database(con, force)
        else:
            migrate_database(con, version)

    finally:
        con.close()
    return True


def create_database(con: sqlite3.Connection, drop: bool = False):
    """Creates the playlist table."""
    with con:
        if drop:
            print("[bold red]Dropping existing table!")
            con.execute('DROP TABLE "playlists"')
        con.execute(
            """CREATE TABLE "playlists" (
                "key"       INTEGER NOT NULL,
                "id"        TEXT NOT NULL UNIQUE,
                "url"       TEXT NOT NULL,
                "title"     TEXT,
                "date"      TEXT,
                "priority"  INTEGER DEFAULT 0,
                "enabled"   INTEGER DEFAULT 1,
                "limit"     TEXT,
                PRIMARY KEY("key")
            );"""
        )
        # using parameters on this command errors out
        con.execute(f"PRAGMA user_version={MIGRATION}")


def migrate_database(con: sqlite3.Connection, version: int):
    with con:
        match version:
            case 0:
                pass
            case 1:
                pass
            case _:
                raise SchemaError(f"Unknown schema version {version}")
        # using parameters on this command errors out
        con.execute(f"PRAGMA user_version={MIGRATION}")


def init_config(path: str, force: bool):
    if not os.path.exists(path) or force:
        create_config(path)


def create_config(path: str):
    opts = cli_to_api(
        shlex.split(
            """-S 'res:1080' --extractor-args "youtubetab:approximate_date" -o "%(playlist)s/%(playlist_index)03d - %(title)s [%(id)s].%(ext)s" -P "temp:.playfetch/temp" --embed-metadata --embed-subs --sponsorblock-mark all --sleep-requests 1.25 --sleep-interval 60 --max-sleep-interval 90 --mtime --break-per-input --no-progress --no-color --no-abort-on-error"""
        ),
        False,
    )
    with open(path, "w") as fp:
        tomlkit.dump(strip_dict(opts), fp)


def strip_dict(data):
    """
    Workaround for:
    https://github.com/sdispater/tomlkit/issues/240
    """
    new_data = {}

    for k, v in data.items():
        if isinstance(v, dict):
            v = strip_dict(v)
        elif isinstance(v, (list, set)):
            v = strip_list(v)
        if v not in (None, {}):
            new_data[k] = v
    return new_data


def strip_list(data: list | set):
    new_list = []

    for v in data:
        if isinstance(v, dict):
            v = strip_dict(v)
        elif isinstance(v, (list, set)):
            v = strip_list(v)
        if v not in (None, {}):
            new_list.append(v)
    return new_list


@app.command()
def diagnose():
    """Prints debug yt-dlp options"""
    # force verbose
    with yt_dlp.YoutubeDL(get_options(os.getcwd(), verbose=True)):
        # just a single (verbose) close is enough
        pass


def get_options(
    path: str,
    /,
    verbose: bool | None = None,
    download_archive: str | None = None,
    **kargs,
):
    with open(get_config_path(path, CONFIG)) as fp:
        options = tomlkit.load(fp)
        download_archive = (
            download_archive
            if download_archive is not None
            else options["download_archive"]
            if "download_archive" in options
            else None
        )
        verbose = (
            verbose
            if verbose is not None
            else options["verbose"]
            if "verbose" in options
            else None
        )
        if verbose:
            download_logger.setLevel(logging.NOTSET)

        return {
            **options,
            "download_archive": download_archive
            if download_archive is not None
            else str(get_config_path(path, ARCHIVE)),
            "verbose": verbose if verbose is not None else state["verbose"] > 2,
            "logger": download_logger,
            **kargs,
        }


@app.command()
def add(
    url: Annotated[str, typer.Argument()],
    title: Annotated[Optional[str], typer.Argument()] = None,
    priority: Annotated[int, typer.Option("--priority", "-p")] = 0,
    disabled: Annotated[
        bool, typer.Option(help="Disables automatic fetching.")
    ] = False,
    limit: Annotated[
        str, typer.Option("--limit", "-l", callback=bytes_validate)
    ] = None,
    force: Annotated[bool, typer.Option(help="Force update (upsert)")] = False,
    ignore: Annotated[bool, typer.Option(help="Ignore fetch errors")] = False,
):
    """Adds a playlist for permanent fetching."""
    cwd = os.getcwd()
    con = get_config(cwd)
    if con is None:
        print("[bold red]ERROR:[/] No config found, please init playfetch first")
        return

    try:
        options = get_options(
            cwd,
            simulate=True,
            extract_flat="in_playlist",
            ignoreerrors=True if ignore else False,
        )
        with yt_dlp.YoutubeDL(options) as ydl:
            info = ydl.sanitize_info(ydl.extract_info(url, download=False))
            if not isinstance(info, Mapping):
                print("[bold red]ERROR:[/] Unknown info found")
                return
            if info["_type"] != "playlist":
                print("[bold red]ERROR:[/] Not a playlist")
                return
            id = get_id(info)
            final_title = title if title else info["title"]
            with con:
                upsert = (
                    """UPDATE SET
                        url=excluded.url,
                        title=excluded.title,
                        priority=excluded.priority,
                        enabled=excluded.enabled,
                        limit=excluded.limit"""
                    if force
                    else "NOTHING"
                )
                updated = con.execute(
                    f"""INSERT INTO "playlists"("id", "url", "title", "priority", "enabled", "limit") 
                        VALUES (?,?,?,?,?,?) 
                        ON CONFLICT("id") DO {upsert}""",
                    (
                        id,
                        info["webpage_url"],
                        final_title,
                        priority,
                        not disabled,
                        limit,
                    ),
                ).rowcount
                if updated:
                    print(f"[green]Added playlist:[/] \\[{id}\\] {escape(final_title)}")
                else:
                    print("[yellow]Playlist already present")
    finally:
        con.close()


def get_config(path: str):
    databasePath = get_config_path(path, DATABASE)
    if not databasePath.exists():
        return None

    return sqlite3.connect(databasePath)


def get_id(playlist_info: dict):
    """returns unique playlist id"""
    return f"{playlist_info['extractor_key']}:{playlist_info['id']}"


@app.command()
def fetch(
    url: Annotated[Optional[List[str]], typer.Argument(show_default=False)] = None,
    indexed: Annotated[bool, typer.Option()] = True,
    reverse: Annotated[bool, typer.Option("--reverse", "-r")] = False,
    throttle: Annotated[str, typer.Option(callback=bytes_validate)] = None,
    limit: Annotated[
        str, typer.Option("--limit", "-l", callback=bytes_validate)
    ] = "8G",
):
    """Fetches playlists (either one-off urls and/or indexed)."""
    cwd = os.getcwd()
    con = get_config(cwd)
    if con is None:
        log.error("No config found, please init playfetch first")
        return

    try:
        rate_limit = parse_bytes(throttle)
        options = get_options(
            cwd,
            ratelimit=rate_limit if rate_limit > 0 else None,
            extract_flat=False,
        )

        main_progress = Progress(
            TextColumn("[progress.description]{task.description}"),
            BarColumn(bar_width=None),
            TaskProgressColumn(),
            MofNCompleteColumn(),
            TimeElapsedColumn(),
            auto_refresh=False,
        )
        playlist_progress = Progress(
            TextColumn("[progress.description]{task.description}"),
            BarColumn(bar_width=None),
            TaskProgressColumn(),
            MofNCompleteColumn(),
            TimeElapsedColumn(),
        )
        download_progress = Progress(
            TextColumn("[progress.description]{task.description}", justify="right"),
            BarColumn(bar_width=None),
            TaskProgressColumn("[progress.percentage]{task.percentage:>3.1f}%"),
            DownloadColumn(),
            TransferSpeedColumn(),
            # "•",
            TimeRemainingColumn(),
        )

        playlist_panel = Panel(
            Group(main_progress, playlist_progress, download_progress)
        )

        urls = url if url is not None else []
        indexes: list[tuple[str, str | None, str | None, str | None]] = con.execute(
            f"""SELECT "url", "key", "title", "limit" FROM "playlists" 
                WHERE "enabled" != 0
                ORDER BY "priority" DESC, "date" {"DESC" if reverse else "ASC"}"""
        ).fetchall()
        playlists = (indexes if indexed else []) + [(x, None, None, "0") for x in urls]
        print(f"[cyan]Downloading {len(playlists)} playlists ({len(indexes)} indexed)")

        main_progress_task = main_progress.add_task(
            "Playlists",
            total=len(playlists),
        )

        # download all playlists
        with Live(playlist_panel, console=console):
            n_videos = 0
            playlist_task: TaskID | None = None
            download_tasks: dict[str, TaskID] = {}
            overrides = {
                "key": None,
                "title": None,
                "limit": None,
            }

            def pre_process(information):
                # update playlist title to obtained from database, if any
                if "playlist" not in information or information["playlist"] is None:
                    raise NotAPlaylist(url)
                if overrides["title"] is not None:
                    information["playlist"] = overrides["title"]

                # initialize playlist task
                # use video playlist index while at it
                assert "playlist_index" in information
                playlist_index = information["playlist_index"]
                assert playlist_index is not None

                if playlist_task is not None:
                    playlist_progress.update(
                        playlist_task,
                        description=information["playlist"],
                        completed=playlist_index - 1,
                        total=information["n_entries"],
                    )

            def report_progress(report: dict):
                """yt-dlp callback function for progress"""
                information: dict = report["info_dict"]

                # need id for any of the following tasks
                # subtitle progress reports don't have the id, lol
                if "id" not in information:
                    return

                # create video task if not created yet
                id = information["id"]
                download_task = download_tasks.get(id)
                if download_task is None:
                    download_task = download_tasks[id] = download_progress.add_task(id)

                status = report["status"]
                if status == "downloading" or status == "finished":
                    total = (
                        report["total_bytes"]
                        if "total_bytes" in report
                        else report["total_bytes_estimate"]
                        if "total_bytes_estimate" in report
                        else None
                    )
                    download_progress.update(
                        download_task,
                        total=total,
                        completed=total
                        if status == "finished"
                        else report["downloaded_bytes"],
                    )
                elif status == "error":
                    download_progress.update(download_task, description=f"[red]{id}")
                    download_progress.stop(download_task)

            def after_video(information):
                # increment downloaded videos
                nonlocal n_videos
                n_videos = n_videos + 1

            def post(title):
                # advance playlist progress bar, both for downloads and skips
                playlist_progress.advance(playlist_task)

            with yt_dlp.YoutubeDL(options) as ydl:
                ydl.add_progress_hook(report_progress)
                ydl.add_post_processor(
                    RunCallbackPP(pre_process),
                    when="pre_process",
                )
                check_size_limit_pp = CheckSizeLimitPP()
                ydl.add_post_processor(check_size_limit_pp, when="before_dl")
                update_upload_date_pp = UpdateUploadDate(con)
                ydl.add_post_processor(update_upload_date_pp)
                ydl.add_post_processor(
                    RunCallbackPP(after_video),
                    when="after_video",
                )
                ydl.add_post_hook(post)

                for url, key, playlist_title, playlist_limit in playlists:
                    overrides["key"] = key
                    overrides["title"] = playlist_title
                    playlist_title = (
                        playlist_title if playlist_title is not None else "?"
                    )
                    playlist_limit = (
                        playlist_limit if playlist_limit is not None else limit
                    )
                    overrides["limit"] = playlist_limit

                    if key is not None:
                        log.info(f"Downloading playlist: {playlist_title}")
                    else:
                        log.info(f"Downloading one-off playlist: {url}")

                    playlist_task = playlist_progress.add_task(
                        playlist_title, total=None, visible=True
                    )
                    update_upload_date_pp.set_key(key)
                    check_size_limit_pp.set_limit(overrides["limit"])

                    try:
                        ydl.extract_info(url)
                    except (DownloadError,) as exc:
                        exc_info = exc.exc_info if exc.exc_info is not None else exc
                        log.error(exc, exc_info=exc_info)
                    except (NotAPlaylist,) as exc:
                        log.error(exc.msg)
                    except (DownloadCancelled,) as exc:
                        log.warn(exc.msg)

                    playlist_progress.remove_task(playlist_task)
                    for task in download_tasks.values():
                        download_progress.remove_task(task)
                    download_tasks.clear()

                    main_progress.advance(main_progress_task, 1)
                    main_progress.refresh()

            # clear temporary empty folders
            if "paths" in options and "temp" in options["paths"]:
                temp_path = options["paths"]["temp"]
                assert temp_path is not None
                deleted = delete_empty_folders(temp_path)
                log.info(f"Deleted {len(deleted)} temp folder(s)")

    finally:
        print(f"[green]{n_videos} {'videos' if n_videos != 1 else 'video'} downloaded")
        con.close()


def parse_bytes(bytestr: str | None) -> int:
    """Parse a string indicating a byte quantity into an integer."""

    if bytestr is None:
        return 0
    return parse_filesize(bytestr if bytestr.endswith(("b", "B")) else bytestr + "iB")


def delete_empty_folders(root):
    """https://stackoverflow.com/a/65624165"""
    deleted = set()

    for current_dir, subdirs, files in os.walk(root, topdown=False):
        still_has_subdirs = False
        for subdir in subdirs:
            if os.path.join(current_dir, subdir) not in deleted:
                still_has_subdirs = True
                break

        if not any(files) and not still_has_subdirs:
            os.rmdir(current_dir)
            deleted.add(current_dir)

    return deleted


if __name__ == "___main___":
    app()
